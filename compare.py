import ReadingExcel
import quickstart
import re
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt


longYearMonths = [31,29,31,30,31,30,31,31,30,31,30,31]
shortYearMonths = [31,28,31,30,31,30,31,31,30,31,30,31]

def main():
    ###Asks the user to input the year of which they would like to see the data from
    ###The years are from 2016 - 2019, because if the user wants to compare the data
    ###Excel files from other years dont exist
    print("Which years data you want to see?")
    yearTemp = input()
    year = int(yearTemp)
    while (year < 2016 or year > 2030):
        print("Which years data you want to see?")
        yearTemp = input()
        year = int(yearTemp)

    ###Asks the user to choose the specific time period, if they do not want to choose specific time periods
    ###then it will just choose the whole year to read data 
    ###1.Method - user inputs specific starting month and end month
    ###2.Method - user chooses quarters of a year, for example, 1st quarter is from January till March
    ###3.Method - user chooses halfs of a year, for example, 1st half is from January till June
    print("Would you like to get data from specific time period? (y/n)")
    methodTrue = input()
    if(methodTrue == 'y'):
        print("Choose a method how you want to read all the events from calendar")
        print("1. From x month to y month")
        print("2. From quarters of a year")
        print("3. After which half of a year")
        method = input()
        methodInt = int (method)
        while(methodInt < 1 or methodInt > 3):
            print("1. From x month to y month")
            print("2. From quarters of a year")
            print("3. After which half of a year")
            method = input()
            methodInt = int (method)

        if(methodInt == 1):
            print("Which is going to be the starting month?(Enter a number for a month)")
            startMonth = input()
            startMonthInt = int(startMonth)
                    
            print("Which is going to be the ending month?(Enter a number for a month)")
            endMonth = input()
            endMonthInt = int(endMonth)
            while(startMonthInt > endMonthInt or startMonth == 12):
                print("Which is going to be the starting month?(Enter a number for a month)")
                startMonth = input()
                startMonthInt = int(startMonth)
                    
                print("Which is going to be the ending month?(Enter a number for a month)")
                endMonth = input()
                endMonthInt = int(endMonth)

        elif(methodInt == 2):
            print("Which quarter of a year would you like to choose(input a number between 1 and 4)")
            quarter = input()
            quarterInt = int(quarter)
            while(quarterInt < 1 or quarterInt > 4):
                print("Which quarter of a year would you like to choose(input a number between 1 and 4)")
                quarter = input()
                quarterInt = int(quarter)

            if(quarterInt == 1):
                startMonthInt = 1
                endMonthInt = 3
            elif(quarterInt == 2):
                startMonthInt = 4
                endMonthInt = 6
            elif(quarterInt == 3):
                startMonthInt = 7
                endMonthInt = 9
            elif(quarterInt == 4):
                startMonthInt = 10
                endMonthInt = 12

        elif(methodInt == 3):
            print("Which half of a year would you like to choose(input 1 or 2)")
            half = input()
            halfInt = int(half)
            while(halfInt < 1 or halfInt > 2):
                print("Which half of a year would you like to choose(input 1 or 2)")
                half = input()
                halfInt = int(half)
                    
            if(halfInt == 1):
                startMonthInt = 1
                endMonthInt = 6
            elif(halfInt == 2):
                startMonthInt = 7
                endMonthInt = 12
    else:
        startMonthInt = 1
        endMonthInt = 12

    ###Asks the user if they would like to see data reads from specific projects, if they say no
    ###then they will get data reads from all projects
    print("Would you like to see specific projects ?(y/n)")
    projectTrue = input()

    if(projectTrue == 'y'):
        print("Which project would you like to see?")
        print("1.RA")
        print("2.VIRAC")
        print("3.EVN")
        print("4.MASER")
        print("5.AGN")
        projectSpecific = input()
        projectInt = int(projectSpecific)
    else:
        projectInt = None

    ###Calculates the total minutes in the specific period, to help calculate how much of that time
    ###the telescopes were used
    totalDays = 0
    if(year%4 != 0):
        ###If it is not a long year
        for x in range (startMonthInt,endMonthInt+1):
            totalDays += shortYearMonths[x-1]

        totalHours = totalDays*24
        totalMinutes = totalHours*60
    else:
        ###If it is a long year
        for x in range(startMonthInt,endMonthInt+1):
            totalDays += longYearMonths[x-1]
        totalHours = totalDays*24
        totalMinutes = totalHours*60

    ###Asks the user which method for data reading they would like
    ###1.Method - user only reads data from the excel
    ###2.Method - user only reads data from calendar
    ###3.Method - user compares the data from excel and calendar
    print("Choose a method for reading data:")
    print("1.Reading data from excel")
    print("2.Reading data from Calendar")
    print("3.Comparing data from both reads")
    readMethod = input()
    readMethodInt = int(readMethod)

    if(readMethodInt == 1):
        ReadingExcel.getSheetFromExcel(year, startMonthInt, endMonthInt, projectInt, True)
    elif(readMethodInt == 2):
        quickstart.getEvents(year,startMonthInt, endMonthInt, projectInt, True)
    elif(readMethodInt == 3):
        print("--------------Reading Excel--------------")
        ReadingExcel.getSheetFromExcel(year, startMonthInt, endMonthInt, projectInt, False)
        ExcelEvents = ReadingExcel.getAllReadsFromExcel()
        
        print("------------Reading Google Doc-----------")
        quickstart.getEvents(year, startMonthInt, endMonthInt, projectInt, False)
        CalendarEvents = quickstart.getAllEventsFromCalendar()
        
        print("Event count in Excel: ",len(ExcelEvents))
        print("Event count in Google: ",len(CalendarEvents))
        makingComparison(ExcelEvents, CalendarEvents, year)

    ###Asks the user if they would like to see a bar chart with the time that operators
    ###spent on all the events
    print("Would you like to see bar chart for operator spent time on events ? (y/n)")
    chartAnswer = input()

    ###To make any charts the names have to be in type "tuple" that is why parsing from array to tuple happens
    if(chartAnswer == 'y'):
        ###Makes the necessary variables to make the charts
        tempExcelOperators = ReadingExcel.getAllOperators()
        tempCalendarOperators = quickstart.getAllOperators()
        operatorNamesList = []
        hourList = []
        calendarOperatorList = []
        calendarHourList = []

        ###Appends the excel arrays to make the charts
        for x in range(len(tempExcelOperators)):
            if(tempExcelOperators[x][1] > 0):
                operatorNamesList.append(tempExcelOperators[x][0])
                tempHours = tempExcelOperators[x][1]/60
                hourList.append(tempHours)

        ###Appends the calendar arrays to make the charts
        for x in range(len(tempCalendarOperators)):
            if(tempCalendarOperators[x][1] > 0):
                calendarOperatorList.append(tempCalendarOperators[x][0])
                tempHours = tempCalendarOperators[x][1]/60
                calendarHourList.append(tempHours)

        ###Makes the chart only if the method wasnt reading only from calendar
        if(readMethodInt != 2):
            operatorNames = tuple(operatorNamesList)
            y_pos = np.arange(len(operatorNames))
            plt.figure("Excel Operator workload")
            plt.bar(y_pos,hourList, align='center', alpha=0.5)
            plt.xticks(y_pos,operatorNames)
            plt.ylabel("Hours")
            plt.title("Operator workload")

        ###Makes the chart only if the method wasnt reading only from excel
        if(readMethodInt != 1):
            calendarOperatorNames = tuple(calendarOperatorList)
            y_pos = np.arange(len(calendarOperatorNames))
            plt.figure("Calendar Operator workload")
            plt.bar(y_pos,calendarHourList, align='center', alpha=0.5)
            plt.xticks(y_pos,calendarOperatorNames)
            plt.ylabel("Hours")
            plt.title("Operator workload")

        plt.show()

    ###Asks the user if they would like to see a bar chart for time spent on each events
    if(projectInt == None):
        print("Would you like to see bar chart for time spent on each event? (y/n)")
        eventAnswer = input()
        if(eventAnswer == 'y'):
            ###Necessary variables
            tempExcelEvents = ReadingExcel.getAllEventsFromExcel()
            tempCalendarEvents = quickstart.getAllEvents()
            calendarEventNameList = []
            excelEventNameList = []
            timeSpentList = []
            calendarTimeSpentList = []
            
            ###Appends the excel arrays to make the charts
            for x in range(len(tempExcelEvents)):
                if(tempExcelEvents[x][1] > 0):
                    excelEventNameList.append(tempExcelEvents[x][0])
                    tempHour = tempExcelEvents[x][2]/60
                    timeSpentList.append(tempHour)

            ###Appends the calendar arrays to make the charts
            for x in range(len(tempCalendarEvents)):
                if(tempCalendarEvents[x][1] > 0):
                    calendarEventNameList.append(tempCalendarEvents[x][0])
                    tempHour = tempCalendarEvents[x][2]/60
                    calendarTimeSpentList.append(tempHour)

            ###Makes the chart only if the method wasnt reading only from calendar
            if(readMethodInt != 2):
                eventNames = tuple(excelEventNameList)
                y_pos = np.arange(len(eventNames))
                plt.figure("Excel event workload")
                plt.bar(y_pos, timeSpentList, align = 'center', alpha=0.5)
                plt.xticks(y_pos,eventNames)
                plt.ylabel("Hours")
                plt.title("Event Workload")

            ###Makes the chart only if the method wasnt reading only from excel
            if(readMethodInt != 1):
                calendarNames = tuple(calendarEventNameList)
                y_pos = np.arange(len(calendarNames))
                plt.figure("Calendar event workload")
                plt.bar(y_pos, calendarTimeSpentList, align='center', alpha = 0.5)
                plt.xticks(y_pos,calendarNames)
                plt.ylabel("Hours")
                plt.title("Event Workload")
            plt.show()

        ###Asks the user if they would like to see bar chart for the amount of each events
        print("Would you like to see bar chart for the amount of each event ? (y/n)")
        amountAnswer = input()
        if(amountAnswer == 'y'):
            ###Necessary variables
            tempExcelEvents = ReadingExcel.getAllEventsFromExcel()
            tempCalendarEvents = quickstart.getAllEvents()
            calendarEventNameList =[]
            calendarEventAmount = []
            excelEventNameList = []
            excelEventAmount = []

            ###Appends the excel arrays to make the charts
            for x in range(len(tempExcelEvents)):
                if(tempExcelEvents[x][1] > 0):
                    excelEventNameList.append(tempExcelEvents[x][0])
                    excelEventAmount.append(tempExcelEvents[x][1])

            ###Appends the calendar arrays to make the charts
            for x in range(len(tempCalendarEvents)):
                if(tempCalendarEvents[x][1] > 0):
                    calendarEventNameList.append(tempCalendarEvents[x][0])
                    calendarEventAmount.append(tempCalendarEvents[x][1])

            ###Makes the chart only if the method wasnt reading only from calendar
            if(readMethodInt != 2):
                excelEventNames = tuple(excelEventNameList)
                y_pos = np.arange(len(excelEventNames))
                plt.figure("Excel event amount")
                plt.bar(y_pos, excelEventAmount, align ='center', alpha = 0.5)
                plt.xticks(y_pos,excelEventNames)
                plt.ylabel("Amount")
                plt.title("Event amount")

            ###Makes the chart only if the method wasnt reading only from excel
            if(readMethodInt != 1):
                calendarEventNames = tuple(calendarEventNameList)
                y_pos = np.arange(len(calendarEventNames))
                plt.figure("Calendar event amount")
                plt.bar(y_pos, calendarEventAmount, align='center', alpha=0.5)
                plt.xticks(y_pos,calendarEventNames)
                plt.ylabel("Amount")
                plt.title("Event amount")

            plt.show()

    ###Asks the user if they would like to see a pie chart for the usage of telescopes
    print("Would you like to see a pie chart for the usage of telescopes?(y/n)")
    pieAnswer = input()
    if(pieAnswer == 'y'):
        ###Makes a pie chart if the method wasnt reading only from calendar
        if(readMethodInt != 2):
            labels = 'RT-32','RT-16', 'steady'

            ###Calculates the time in percentage to make a more accurate chart
            usedMinutes = ReadingExcel.getTotalMinutesForEvents(projectInt)
            usedTelescope = ReadingExcel.getTelescopeUsage()
            usedMinutesPerc = usedMinutes/totalMinutes *100

            RT32perc = usedTelescope[0][1]/usedMinutes * usedMinutesPerc
            RT16perc = usedTelescope[1][1]/usedMinutes * usedMinutesPerc

            steadyMinutes = totalMinutes - usedMinutes
            steadyMinutesPerc = steadyMinutes/totalMinutes *100

            ###Sets up all the variables to make a pie chart
            sizes = [RT32perc, RT16perc , steadyMinutesPerc]
            fig1, ax1 = plt.subplots()
            ax1.pie(sizes, labels = labels, autopct='%1.1f%%',startangle=90)
            ax1.axis('equal')

        ###Makes a pie chart if the method wasnt reading only from excel
        if(readMethodInt != 1):
            labels = 'RT-32','RT-16','No-data','steady'

            ###Calculates the time in percentage to make a more accurate chart
            usedMinutes = quickstart.getTotalMinutesFromEvents(projectInt)
            usedTelescopes = quickstart.getTelescopeUsageFromCalendar()
            usedMinutesPerc = usedMinutes/totalMinutes * 100
            RT32perc = usedTelescopes[0][1]/usedMinutes * usedMinutesPerc
            RT16perc = usedTelescopes[1][1]/usedMinutes * usedMinutesPerc
            noDATAperc = usedTelescopes[2][1]/usedMinutes * usedMinutesPerc

            steadyMinutes = totalMinutes - usedMinutes
            steadyMinutesPerc = steadyMinutes/totalMinutes * 100

            ###Sets up all the variables to make a pie chart
            sizes = [RT32perc, RT16perc, noDATAperc,steadyMinutesPerc]
            fig1, ax1 = plt.subplots()
            ax1.pie(sizes,labels = labels, autopct='%1.1f%%',startangle = 90)
            ax1.axis('equal')
            
        plt.show()

###Function that compares the data that were gotten from reading excel files and google calendar
def makingComparison(ExcelEvents, CalendarEvents, year):
    allEventCount = len(ExcelEvents) + len(CalendarEvents)

    ###Variables to see which event from which method is at top(is happening later than the current one from the other method)
    isCalendarEventTop = False
    isExcelEventTop = True
    excelEventIndex = 0
    calendarEventIndex = 0
    MatchedEvents = 0
    ###Setting the main variables as Excels first events start date and time
    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[0][0])
    topDay = ReadingExcel.getDaysFromString(ExcelEvents[0][0])
    topHour = ReadingExcel.getHoursFromString(ExcelEvents[0][1])
    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[0][1])
    topProject = ExcelEvents[0][4]

    topStatus = str(ExcelEvents[0][5])
    
    method = True
    
    ###Setting the date for the specific timezone
    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
    tempTopMonth = tempList[0]
    tempTopDay = tempList[1]
    tempTopHour = tempList[2]

    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
    tempTopMonth2 = tempList2[0]
    tempTopDay2 = tempList2[1]
    tempTopHour2 = tempList2[2]

    ###Setting end date from excel's first event
    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[0][2])
    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[0][2])
    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[0][3])
    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[0][3])

    ###Setting end date for other timezones
    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
    listTopEndMonth = tempListEnd[0]
    listTopEndDay = tempListEnd[1]
    listTopEndHour = tempListEnd[2]

    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
    listTopEndMonth2 = tempListEnd2[0]
    listTopEndDay2 = tempListEnd2[1]
    listTopEndHour2 = tempListEnd2[2]
    
    try:
        for index in range (allEventCount):
            ###Does all the comparison if at top event is from excel
            if(isExcelEventTop):
                tempMonthStart = quickstart.getMonthFromEvent(CalendarEvents[calendarEventIndex][0])
                tempDayStart = quickstart.getDayFromEvent(CalendarEvents[calendarEventIndex][0])
                tempHourStart = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][0])
                tempMinutesStart = quickstart.getMinutesFromEvent(CalendarEvents[calendarEventIndex][0])

                tempMonthEnd = quickstart.getMonthFromEvent(CalendarEvents[calendarEventIndex][1])
                tempDayEnd = quickstart.getDayFromEvent(CalendarEvents[calendarEventIndex][1])
                tempHourEnd = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][1])
                tempMinutesEnd = quickstart.getMinutesFromEvent(CalendarEvents[calendarEventIndex][1])

                tempStatus = str(CalendarEvents[calendarEventIndex][3])
                ###Checks if the time from one event is the same as the other one as well as their time when
                ###the event ends, checks if it is the same project and if the status matches
                ###If everything matches then the index for both array's goes up by 1 and sets all the variables
                if(topMonth == tempMonthStart and
                    topDay == tempDayStart and
                    topHour == tempHourStart and
                    topMinutes == tempMinutesStart and
                    projectMatch(topProject, CalendarEvents[calendarEventIndex][2]) and
                    topEndMonth == tempMonthEnd and
                    topEndDay == tempDayEnd and
                    topEndHour == tempHourEnd and
                    topEndMinutes == tempMinutesEnd
                    and
                    projectStatusMatch(topStatus, tempStatus)):
                    
                    print("Matches!")
                    MatchedEvents += 1
                    excelEventIndex += 1
                    calendarEventIndex += 1
                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus = ExcelEvents[excelEventIndex][5]

                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]

                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]
                    
                ###Checks if the time from one event is the same as the other one, with a slight difference that
                ###the time has been changed to a different timezone, as well as their time when
                ###the event ends, checks if it is the same project and if the status matches
                ###If everything matches then the index for both array's goes up by 1 and sets all the variables
                elif(tempTopMonth == tempMonthStart and
                    tempTopDay == tempDayStart and
                    tempTopHour == tempHourStart and
                    topMinutes == tempMinutesStart and
                    projectMatch(topProject, CalendarEvents[calendarEventIndex][2]) and
                    listTopEndMonth == tempMonthEnd and
                    listTopEndDay == tempDayEnd and
                    listTopEndHour == tempHourEnd
                    and
                    projectStatusMatch(topStatus, tempStatus)):

                    print("Matches!")
                    MatchedEvents += 1
                    excelEventIndex += 1
                    calendarEventIndex += 1
                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus = ExcelEvents[excelEventIndex][5]
 
                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]

                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]
                    
                ###Checks if the time from one event is the same as the other one, with a slight difference that
                ###the time has been changed to a different timezone, as well as their time when
                ###the event ends, checks if it is the same project and if the status matches
                ###If everything matches then the index for both array's goes up by 1 and sets all the variables
                elif(tempTopMonth2 == tempMonthStart and
                    tempTopDay2 == tempDayStart and
                    tempTopHour2 == tempHourStart and
                    topMinutes == tempMinutesStart and
                    projectMatch(topProject, CalendarEvents[calendarEventIndex][2]) and
                    listTopEndMonth2 == tempMonthEnd and
                    listTopEndDay2 == tempDayEnd and
                    listTopEndHour2 == tempHourEnd
                    and
                    projectStatusMatch(topStatus, tempStatus)):
        
                    print("Matches!")
                    MatchedEvents += 1
                    excelEventIndex += 1
                    calendarEventIndex += 1
                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus = ExcelEvents[excelEventIndex][5]

                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]

                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]

                ###If the events dont match then checks which one of the events is happening at a later date
                ###if the current at top event is happening earlier than the other then changes *top* variables
                ###to the one that is currently happening at a later date
                elif(topMonth < tempMonthStart or
                        topDay < tempDayStart or
                    (topDay == tempDayStart and
                        topHour < tempHourStart) or
                        (topDay == tempDayStart and
                        topHour == tempHourStart and
                        topMinutes < tempMinutesStart)):

                    
                    print("Missed event from excel at: ",year,"-",topMonth,"-",topDay," ",topHour,":",topMinutes)

                    topMonth = quickstart.getMonthFromEvent(CalendarEvents[calendarEventIndex][0])
                    topDay = quickstart.getDayFromEvent(CalendarEvents[calendarEventIndex][0])
                    topHour = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][0])
                    topMinutes = quickstart.getMinutesFromEvent(CalendarEvents[calendarEventIndex][0])
                    topProject = CalendarEvents[calendarEventIndex][2]

                    topStatus = str(CalendarEvents[calendarEventIndex][3])

                    method = False
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,3)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,3)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]
                    ###Setting end date from excel's first event
                    topEndMonth = quickstart.getMonthFromEvent(CalendarEvents[calendarEventIndex][1])
                    topEndDay = quickstart.getDayFromEvent(CalendarEvents[calendarEventIndex][1])
                    topEndHour = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][1])
                    topEndMinutes = quickstart.getMinutesFromEvent(CalendarEvents[calendarEventIndex][1])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,4)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,4)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]
                    isCalendarEventTop = True
                    isExcelEventTop = False
                    excelEventIndex += 1

                ###If the current at top event is happening at a later date then nothing changes and the
                ###other methods index goes up by 1
                else:
                    print("Missed event from calendar at: ",year,
                            "-",quickstart.getMonthFromEvent(CalendarEvents[calendarEventIndex][0]),
                            "-",quickstart.getDayFromEvent(CalendarEvents[calendarEventIndex][0]),
                            " ",quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][0]),
                            ":",quickstart.getMinutesFromEvent(CalendarEvents[calendarEventIndex][0]))
                    calendarEventIndex += 1

            ###Does the comparison if the at top event is from calendar
            elif(isCalendarEventTop):
                tempMonthStart = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                tempDayStart = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                tempHourStart = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                tempMinutesStart = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])

                tempMonthEnd = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                tempDayEnd = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                tempHourEnd = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                tempMinutesEnd = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                tempExcelStatus = str(ExcelEvents[excelEventIndex][5])
                tempCalendarStatus = str(CalendarEvents[calendarEventIndex][3])
                ###Checks if the time from one event is the same as the other one as well as their time when
                ###the event ends, checks if it is the same project and if the status matches
                ###If everything matches then the index for both array's goes up by 1 and sets all the variables
                if(topMonth == tempMonthStart and
                    topDay == tempDayStart and
                    topHour == tempHourStart and
                    topMinutes == tempMinutesStart and
                    projectMatch(topProject, ExcelEvents[excelEventIndex][4]) and
                    topEndMonth == tempMonthEnd and
                    topEndDay == tempDayEnd and
                    topEndHour == tempHourEnd and
                    topEndMinutes == tempMinutesEnd
                    and
                    projectStatusMatch(topStatus, tempExcelStatus)):

                    print("Matches!")
                    MatchedEvents += 1
                    excelEventIndex += 1
                    calendarEventIndex += 1
                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus =ExcelEvents[excelEventIndex][5]

                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]
                    isExcelEventTop = True
                    isCalendarEventTop = False
                    
                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]

                ###Checks if the time from one event is the same as the other one, with a slight difference that
                ###the time has been changed to a different timezone, as well as their time when
                ###the event ends, checks if it is the same project and if the status matches
                ###If everything matches then the index for both array's goes up by 1 and sets all the variables
                elif(tempTopMonth == tempMonthStart and
                    tempTopDay == tempDayStart and
                    tempTopHour == tempHourStart and
                    topMinutes == tempMinutesStart and
                    projectMatch(topProject, ExcelEvents[excelEventIndex][4]) and
                    listTopEndMonth == tempMonthEnd and
                    listTopEndDay == tempDayEnd and
                    listTopEndHour == tempHourEnd and
                    topEndMinutes == tempMinutesEnd
                    and
                    projectStatusMatch(topStatus, tempCalendarStatus)):
                    
                    print("Matches!")
                    MatchedEvents += 1
                    excelEventIndex += 1
                    calendarEventIndex += 1
                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus = ExcelEvents[excelEventIndex][5]

                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]

                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]
                    
                    isExcelEventTop = True
                    isCalendarEventTop = False

                ###Checks if the time from one event is the same as the other one, with a slight difference that
                ###the time has been changed to a different timezone, as well as their time when
                ###the event ends, checks if it is the same project and if the status matches
                ###If everything matches then the index for both array's goes up by 1 and sets all the variables
                elif(tempTopMonth2 == tempMonthStart and
                    tempTopDay2 == tempDayStart and
                    tempTopHour2 == tempHourStart and
                    topMinutes == tempMinutesStart and
                    projectMatch(topProject, ExcelEvents[excelEventIndex][4]) and
                    listTopEndMonth2 == tempMonthEnd and
                    listTopEndDay2 == tempDayEnd and
                    listTopEndHour2 == tempHourEnd and
                    topEndMinutes == tempMinutesEnd and
                    projectStatusMatch(topStatus, tempCalendarStatus)):

                    print("Matches!")
                    MatchedEvents += 1
                    excelEventIndex += 1
                    calendarEventIndex += 1
                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus = ExcelEvents[excelEventIndex][5]

                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]

                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]
                    
                    isExcelEventTop = True
                    isCalendarEventTop = False

                ###If the events dont match then checks which one of the events is happening at a later date
                ###if the current at top event is happening earlier than the other then changes *top* variables
                ###to the one that is currently happening at a later date
                elif(topMonth < tempMonthStart or
                        topDay < tempDayStart or
                    (topDay == tempDayStart and
                        topHour < tempHourStart) or
                        (topDay == tempDayStart and
                        topHour == tempHourStart and
                        topMinutes < tempMinutesStart)):

                    print("Missed event from calendar at: ",year,"-",topMonth,"-",topDay," ",topHour,":",topMinutes)

                    topMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0])
                    topDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0])
                    topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1])
                    topMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1])
                    topProject = ExcelEvents[excelEventIndex][4]

                    topStatus =ExcelEvents[excelEventIndex][5]

                    method = True
                    ###Setting the date for the specific timezone
                    tempList = addHoursToEvent(topMonth, topDay, topHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth = tempList[0]
                    tempTopDay = tempList[1]
                    tempTopHour = tempList[2]

                    ###Setting the date for other timezone
                    tempList2 = removeHoursFromEvent(topMonth, topDay, topHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,1)
                    tempTopMonth2 = tempList2[0]
                    tempTopDay2 = tempList2[1]
                    tempTopHour2 = tempList2[2]

                    ###Setting end date from excel's first event
                    topEndMonth = ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][2])
                    topEndDay = ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][2])
                    topEndHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][3])
                    topEndMinutes = ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][3])

                    ###Setting end date for other timezones
                    tempListEnd = addHoursToEvent(topEndMonth, topEndDay, topEndHour, 1, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth = tempListEnd[0]
                    listTopEndDay = tempListEnd[1]
                    listTopEndHour = tempListEnd[2]

                    tempListEnd2 = removeHoursFromEvent(topEndMonth, topEndDay, topEndHour, 2, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex,2)
                    listTopEndMonth2 = tempListEnd2[0]
                    listTopEndDay2 = tempListEnd2[1]
                    listTopEndHour2 = tempListEnd2[2]
                    
                    isCalendarEventTop = False
                    isExcelEventTop = True
                    calendarEventIndex += 1
                    
                ###If the current at top event is happening at a later date then nothing changes and the
                ###other methods index goes up by 1
                else:
                    print("Missed event from Excel at: ",year,
                            "-",ReadingExcel.getMonthFromString(ExcelEvents[excelEventIndex][0]),
                            "-",ReadingExcel.getDaysFromString(ExcelEvents[excelEventIndex][0]),
                            " ",ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][1]),
                            ":",ReadingExcel.getMinutesFromString(ExcelEvents[excelEventIndex][1]))
                    excelEventIndex += 1
                    

    except Exception as e:
        print(e)

    print("Total count for matched events: ",MatchedEvents)
    print("Missed event in google calendar: ",len(CalendarEvents) - MatchedEvents)
    print("Missed events in Excel: ",len(ExcelEvents) - MatchedEvents)

###Adds hours to the specific event to check if the other events timezone matches this one
def addHoursToEvent(topMonth, topDay, topHour, index, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex, methodIndex):
    isLongYear = False
    
    ###Sets the index on which it should take the hours from
    ###1 - start time for excel
    ###2 - end time for excel
    ###3 - start time for google event
    ###4 - end time for google event
    if(methodIndex == 1):
        time = 1
    elif(methodIndex == 2):
        time = 3
    elif(methodIndex == 3):
        time = 0
    elif(methodIndex == 4):
        time = 1
    if(year %4 == 0):
        isLongYear = True
    if(method == True):
        if(ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][time]) + index >= 24):
            if(isLongYear == False):
                if(topDay + index > shortYearMonths[topMonth-1]):
                    topDay = topDay + index - shortYearMonths[topMonth-1]
                    topMonth += 1
                else:
                    topDay += 1
            else:
                if(topDay + index > longYearMonths[topMonth-1]):
                    topDay = topDay + index - longYearMonths[topMonth-1]
                    topMonth += 1
                else:
                    topDay += 1
            topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][time]) + index - 24
        else:
            topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][time]) + index

    else:
        if(quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][time]) + index >= 24):
            if(isLongYear == False):
                if(topDay + index > shortYearMonths[topMonth-1]):
                    topDay = topDay + index - shortYearMonths[topMonth-1]
                    topMonth += 1
                else:
                    topDay += 1
            else:
                if(topDay + index > longYearMonths[topMonth-1]):
                    topDay = topDay + index - longYearMonths[topMonth-1]
                    topMonth += 1
                else:
                    topDay += 1
            topHour = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][time]) + index - 24
        else:
            topHour = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][time]) + index

    return topMonth, topDay, topHour

###removes hours to the specific event to check if the other events timezone matches this one
def removeHoursFromEvent(topMonth, topDay, topHour, index, year, method, ExcelEvents, excelEventIndex, CalendarEvents, calendarEventIndex, methodIndex):
    longYearMonths = [31,29,31,30,31,30,31,31,30,31,30,31]
    shortYearMonths = [31,28,31,30,31,30,31,31,30,31,30,31]
    isLongYear = False
    ###Sets the index on which it should take the hours from
    ###1 - start time for excel
    ###2 - end time for excel
    ###3 - start time for google event
    ###4 - end time for google event
    if(methodIndex == 1):
        time = 1
    elif(methodIndex == 2):
        time = 3
    elif(methodIndex == 3):
        time = 0
    elif(methodIndex == 4):
        time = 1 
    if(year %4 == 0):
        isLongYear = True
    if(method == True):
        if(ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][time]) - index < 0):
            if(isLongYear == False):
                if(topDay - 1 < 0):
                    topDay = topDay - 1 + shortYearMonths[topMonth-1]
                    topMonth -= 1
                else:
                    topDay -= 1
            else:
                if(topDay - 1 < 0):
                    topDay = topDay - 1 + longYearMonths[topMonth-1]
                    topMonth += 1
                else:
                    topDay -= 1
            topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][time]) - index + 24
        else:
            topHour = ReadingExcel.getHoursFromString(ExcelEvents[excelEventIndex][time]) - index
    else:
        if(quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][time]) - index < 0):
            if(isLongYear == False):
                if(topDay - 1 < 0):
                    topDay = topDay - 1 + shortYearMonths[topMonth-1]
                    topMonth -= 1
                else:
                    topDay -= 1
            else:
                if(topDay - 1 < 0):
                    topDay = topDay - 1 + longYearMonths[topMonth-1]
                    topMonth += 1
                else:
                    topDay -= 1
            topHour = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][time]) - index + 24
        else:
            topHour = quickstart.getHoursFromEvent(CalendarEvents[calendarEventIndex][time]) - index

    return topMonth, topDay, topHour

###Using regex reads if the specific project is RA
def getRAevent(project):
    if(re.search("^RA",project) or
       re.search(" RA ",project)):
        return True
    else:
        return False

###reads if the specific project is EVN
def getEVNevent(project):
    if(re.search("EVN",project)):
        return True
    else:
        return False

###Reads if the specific project is VIRAC
def getVIRACevent(project):
    if(re.search("VIRAC",project)):
        return True
    else:
        return False

###Reads if the specific project is AGN
def getAGNevent(project):
    if(re.search("AGN",project)):
        return True
    else:
        return False

###Reads if the specific project is maser
def getRT32_MASERevent(project):
    if(re.search("maser",project)):
        return True
    else:
        return False

###Using all the reading methods for projects checks if the projects from both events matches
def projectMatch(excelProject, calendarProject):
    if((getRAevent(excelProject) and getRAevent(calendarProject)) or
       (getEVNevent(excelProject) and getEVNevent(calendarProject)) or
       (getVIRACevent(excelProject) and getVIRACevent(calendarProject)) or
        (getAGNevent(excelProject) and getAGNevent(calendarProject)) or
       (getRT32_MASERevent(excelProject) and getRT32_MASERevent(calendarProject))):
        return True
    if(getVIRACevent(excelProject) and getRT32_MASERevent(calendarProject)):
        return True
    if(getVIRACevent(excelProject) and getAGNevent(calendarProject)):
        return True
    return False

###Reads if the events status was a succes or not
def getSucessStatus(status):
    if(re.search("sucess", status) or
       re.search("Success", status) or
       re.search("Sucess", status) or
       re.search("success", status)):
        return True
    else:
        return False

###Checks if the status from both methods for an event matches
def projectStatusMatch(excelProject, calendarProject):
    if(getSucessStatus(excelProject) and getSucessStatus(calendarProject)):
        return True
    else:
        return False

    
if __name__ == '__main__':
    main()

