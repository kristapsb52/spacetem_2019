import os
import pyexcel as pe
import datetime
import re

###All events that are going to be used for comparison in different script
allReadsInExcel = []

###All operators found in excel files
allOperators = [["K.Prusis", 0],
                ["A.Nikolajevs", 0],
                ["A.Orbidans", 0],
                ["E.Visockis",0],
                ["M.Bleiders", 0],
                ["A.Aberfelds", 0],
                ["Vl.Bezrukovs", 0],
                ["D.Bezrukovs", 0],
                ["R.Rasmanis", 0],
                ["E.Bredikis", 0],
                ["R.Dirda", 0]]

###All events found in excel files
allEvents =[["RA events", 0, 0],
            ["VIRAC events", 0, 0],
            ["EVN events", 0, 0],
            ["maser events", 0, 0],
            ["AGN events", 0, 0],
            ["NKA events", 0, 0],
            ["SUN events", 0, 0],
            ["EOFUS events", 0, 0],
            ["eVLBI events", 0, 0],
            ["IVS events", 0, 0]]

###The usage of the telescopes in specific time periods
telescopeUsage = [["RT-32", 0],
                  ["RT-16", 0]]

def main(year, startMonth, endMonth, specific):
    getSheetFromExcel(year, startMonthInt, endMonthInt, specifics)

###Returns an array with the telescope usage in specific time periods
def getTelescopeUsage():
    return telescopeUsage

###Calculates the total amount of minutes in the specific time period
def getTotalMinutesForEvents(projectInt):
    result = 0
    if(projectInt == None):
        for event in allEvents:
            result += event[2]
    else:
        result = allEvents[projectInt-1][2]
    return result

#Returns an array with all the events
def getAllEventsFromExcel():
    return allEvents

#Returns an array with all the events for comparison in different script
def getAllReadsFromExcel():
    return allReadsInExcel 

#Returns a number for the specific project that is going to be searched for
def getSpecificProject():
    print("Would you like to see specific projects ?(y/n)")
    projectTrue = input()

    if(projectTrue == 'y'):
        print("Would you like to choose 'multiple' or 'single' projects?")
        projectCount = input()
        if(projectCount == 'single'):
            print("Which project would you like to see?")
            print("1.RA")
            print("2.VIRAC")
            print("3.EVN")
            print("4.MASER")
            print("5.AGN")
            print("6.NKA")
            print("7.SUN")
            print("8.EOFUS")
            print("9.eVLBI")
            print("10.IVS")
            projectSpecific = input()
            projectInt = int(projectSpecific)
    else:
        projectInt = None
    return projectInt

#Function that starts all the reads from excel
def getSheetFromExcel(year, startMonth, endMonth, specific, method):
    #Variables for the files
    year2019 = ("Observations_protocol_2019.ods")
    year2018 = ("Observations_protocol_2018.ods")
    year2017 = ("Observations_protocol_2017.ods")
    year2016 = ("Observations_protocol_2016.ods")

    #Makes a sheet for different years and starts reading the data in them
    if(year == 2016):
        book = pe.get_book(file_name = os.path.join(year2016))
        sheet = book.sheet_by_name("2016")
        eventInfo(sheet,2016,startMonth,endMonth, specific, method)
            
    elif(year == 2017):
        book = pe.get_book(file_name = os.path.join(year2017))
        sheet = book.sheet_by_name("2017")
        eventInfo(sheet,2017,startMonth,endMonth, specific, method)
            
    elif(year == 2018):
        book = pe.get_book(file_name = os.path.join(year2018))
        sheet = book.sheet_by_name("2018")
        eventInfo(sheet,2018,startMonth,endMonth, specific, method)
            
    elif(year == 2019):
        book = pe.get_book(file_name = os.path.join(year2019))
        sheet = book.sheet_by_name("Sheet1")
        eventInfo(sheet,2019,startMonth,endMonth, specific, method)

def eventInfo(sheet, year, startMonth, endMonth, specific, method):
###Event variable
    totalTime = 0
    startRow = 2

    ###Sets all the column variables for year 2016, because the columns were different
    ###than all the other files
    if(year == 2016):
        startDateColumn = 0
        startDateTimeColumn = 1
        endDateColumn = 2
        endDateTimeColumn = 3
        projectNameColumn = 4
        telescopeColumn = 7
        operatorColumn = 8
        eventStatusColumn = 13
        ###Gets the starting row if startMonth isnt 1
        if(startMonth != 1):        
            startRow = getStartRow(startMonth, sheet, startDateColumn)

        ###Starts to read all the data with all the column variables
        eventInfoHelper(startRow, startDateColumn, startDateTimeColumn, endDateColumn, endDateTimeColumn,
                        projectNameColumn, telescopeColumn, operatorColumn, eventStatusColumn, sheet, year, allEvents, totalTime,
                        allOperators, startMonth, endMonth, specific, method)
        timeSpentOnEventString(totalTime)

    ###Sets all the column variables for other years
    else:
        ###Column variables
        startDateColumn = 0
        startDateTimeColumn = 1
        endDateColumn = 2
        endDateTimeColumn = 3
        projectNameColumn = 5
        projectSpecNameColumn = 6
        telescopeColumn = 8
        operatorColumn = 9
        eventStatusColumn = 15

        ###Gets the starting row if starting month isnt 1
        if(startMonth != 1):
            startRow = getStartRow(startMonth, sheet, startDateColumn)
        ###Starts to read all the data with all the column variables
        eventInfoHelper(startRow, startDateColumn, startDateTimeColumn, endDateColumn, endDateTimeColumn,
                        projectNameColumn,projectSpecNameColumn,telescopeColumn, operatorColumn, eventStatusColumn, sheet, year, allEvents, totalTime,
                        allOperators, startMonth, endMonth, specific, method)
        timeSpentOnEventString(totalTime)

    #Prints out all the events and their count as well as time spent
##    if(specific == None):
##        for event in allEvents:
##            if(event[1] > 0):
##                print (event[0] + ": ",event[1])
##                print ("Time spent: " + timeSpentOnEventString(event[2]))
##    else:
##        print(allEvents[specific-1][0] + ": ", allEvents[specific-1][1])
##        print("Time spent: " + timeSpentOnEventString(allEvents[specific-1][2]))

    #Prints out all the operators and their time spent on all events
    for operator in allOperators:
        if(operator[1] > 0):
            print(operator[0] + " has been working for: " + timeSpentOnEventString(operator[1]))

#Returns the starting row based on starting month
def getStartRow(startMonth, sheet, startDateColumn):
    startRow = 2
    tempStartDate = str(sheet.cell_value(startRow, startDateColumn))
    tempMonth = getMonthFromString(tempStartDate)
    while(tempMonth != startMonth):
        startRow += 1
        tempStartDate = str(sheet.cell_value(startRow, startDateColumn))
        tempMonth = getMonthFromString(tempStartDate)
    return startRow

###The main function for reading all the data
def eventInfoHelper(startRow, startDateColumn, startDateTimeColumn, endDateColumn, endDateTimeColumn, projectNameColumn,projectSpecNameColumn,telescopeColumn,
                    operatorColumn, eventStatusColumn, sheet, year, allEvents, totalTime, allOperators, startMonth,
                    endMonth, specific, method):
    timeSpent = 0
    Break = 0
    arrayLength = 0
    tempYearCell = str(sheet.cell_value(startRow,startDateColumn))
    currentYear = getYearFromString(tempYearCell)

    while(Break == 0):
        try:
            ###Gets the value from specific row and column
            ###Reads them as strings to know which type to parse it float/int
            ###Uses try and except if it cant parse to one method then uses the other one
            wontWork = 0
            tempStartDate = str(sheet.cell_value(startRow,startDateColumn))
            tempStartDateTime = str(sheet.cell_value(startRow,startDateTimeColumn))

            tempEndDate = str(sheet.cell_value(startRow, endDateColumn))
            tempEndDateTime = str(sheet.cell_value(startRow, endDateTimeColumn))

            ###Parsing starting date
            try:        
                tempInt = int(tempStartDate)
                startDate = getDateFromInt(tempInt)
            except:
                startDate = str(getDateFromRegex(tempStartDate))
                startDate = splitStringDate(startDate)

            ###Gets the current year from starting date
            try:
                currentYear = getYearFromString(startDate)
                if(currentYear != year):
                    wontWork = 1
            except:
                None
            ###Parsing time to know what time it is
            try:
                tempFloat = float(tempStartDateTime)
                if(tempFloat > 1 ):
                    tempTime = str(datetime.timedelta(days = tempFloat))
                    startDateTime = str(getTimeFromRegex(tempTime))
                    startDateTime = splitStringTime(startDateTime)
                else:
                    startDateTime = str(datetime.timedelta(days = tempFloat))
            except:        
                startDateTime = str(getTimeFromRegex(tempStartDateTime))
                startDateTime = splitStringTime(startDateTime)
            

            ###Parsing end date from the event
            try:
                tempInt = int(tempEndDate)
                endDate = getDateFromInt(tempInt)
            except:
                endDate = str(getDateFromRegex(tempEndDate))
                endDate = splitStringDate(endDate)

            ###Parsing event date time from the event, to know at what time it ended
            try:
                tempFloat = float(tempEndDateTime)
                if(tempFloat > 1):
                    tempTime = str(datetime.timedelta(days = tempFloat))
                    endDateTime = str(getTimeFromRegex(tempTime))
                    endDateTime = splitStringTime(endDateTime)
                else:
                    endDateTime = str(datetime.timedelta(days = tempFloat))
            except:
                endDateTime = str(getTimeFromRegex(tempEndDateTime))
                endDateTime = splitStringTime(endDateTime)

            ###Which telescope/operator/status was for the event
            telescope = sheet.cell_value(startRow, telescopeColumn)
            operator = sheet.cell_value(startRow, operatorColumn)
            eventStatus = str(sheet.cell_value(startRow, eventStatusColumn))

            if(type(endDateTime) == float):
                endDateTime = getTimeFromFloat(endDateTime)

            ###If one of the cells was empty makes it so it doesnt continue reading it
            if(tempStartDateTime == "" or tempStartDate == "" or tempEndDateTime == "" or tempEndDate == ""):
                wontWork = 1
            ###Makes it so it doesnt read any other events after the ending month that was inputted by the user
            if(endMonth != None):
                try:
                    tempMonth = getMonthFromString(str(startDate))
                    if(tempMonth < startMonth or tempMonth > endMonth):
                        wontWork = 1
                    else:
                        projectName = str(sheet.cell_value(startRow, projectNameColumn))
                        specProjectName = str(sheet.cell_value(startRow, projectSpecNameColumn))
                        timeSpent = timeSpentOnEvent(startDate, startDateTime, endDate, endDateTime, year)
                        projects = [0,0,0,0,0,0,0,0,0,0]
                        countingEvents(projectName,specProjectName, allEvents, projects, timeSpent)
                        if(specific != None):
                            if(projects[specific-1] == 0):
                                wontWork = 1
                except:
                    wontWork = 1

            ###If all the readings were succesful and there were no errors then continues
            if(wontWork == 0):
                ###If the event was successful only then adds the time spent on the event to operators and events
                if(isEventSuccessful(eventStatus)):
                    timeSpent = timeSpentOnEvent(startDate, startDateTime, endDate, endDateTime, year)
                    countingOperatorsInEvents(allOperators, operator, timeSpent)
                    totalTime += timeSpent
                    ###Adds the time spent for one of the telescopes that were used
                    if(isRT32used(telescope)):
                        telescopeUsage[0][1] += timeSpent
                    elif(isRT16used(telescope)):
                        telescopeUsage[1][1] += timeSpent

                ###Appends the array for comparison in different script
                allReadsInExcel.append([])
                allReadsInExcel[arrayLength].append(startDate)
                allReadsInExcel[arrayLength].append(startDateTime)
                allReadsInExcel[arrayLength].append(endDate)
                allReadsInExcel[arrayLength].append(endDateTime)
                allReadsInExcel[arrayLength].append(projectName)
                allReadsInExcel[arrayLength].append(eventStatus)
                arrayLength += 1

                ###If the user wanted to see all the data that was stored in the file, it prints it out
                if(method == True):
                    print("------------------------")
                    print(startDate,startDateTime," until ",endDate,endDateTime)
                    print("Project: " + projectName + " OP: " + operator + " STATUS: " + eventStatus)
                    print("Time spent: ", timeSpentOnEventString(timeSpent))
            startRow += 1
                
        except Exception as e:
            print(e)
            Break = 1

    ###Prints out the total amount of time spent on all the events
##    print("Total time spent on all events: ",timeSpentOnEventString(totalTime))
##    if(specific != None):
##        print("Total count for ",allEvents[specific-1][0], ": ",allEvents[specific-1][1])
##    print()
                
###Converts int to datetime.date
def getDateFromInt(days):
    longYearMonths = [31,29,31,30,31,30,31,31,30,31,30,31]
    shortYearMonths = [31,28,31,30,31,30,31,31,30,31,30,31]
    startingYear = 1900
    totalDays = 0
    startingNode = 1
    isLongYear = False
    while(totalDays < days):
        if(startingNode %4 == 1):
            totalDays += 366
            if(totalDays > days):
                totalDays -= 366
                startingNode -= 1
                isLongYear = True
                break
        else:
            totalDays += 365
            if(totalDays > days):
                totalDays -= 365
                startingNode -= 1
                break

        startingNode += 1

    year = startingYear + startingNode
    remainingDays = days%totalDays
    month = 0
    if(isLongYear):
        for x in range (len(longYearMonths)):
            if(remainingDays - longYearMonths[x] > 0):
                remainingDays -= longYearMonths[x]
            else:
                month = x+1
                break
    else:
        for x in range (len(shortYearMonths)):    
            if(remainingDays - shortYearMonths[x] > 0):
                remainingDays -= shortYearMonths[x]
            else:
                month = x+1
                break
    testDate = datetime.date(year,month,remainingDays).isoformat()
    return testDate

###Converts float to datetime.time
def getTimeFromFloat(number):
    time = str(datetime.timedelta(days = number))
    return time

###Returns the year from string
def getYearFromString(time):
    try:
        tempString = time[0:4]
        result = int(tempString)
    except:
        tempString = time[6:10]
        result = int(tempString)
    return result

###Returns the month from string
def getMonthFromString(time):
    try:
        tempString = time[5:7]
        result = int(tempString)
    except:
        tempString = time[3:5]
        result = int(tempString)
    return result

###Returns the days from the string, checks what kind of type of date it was to correctly return the days
def getDaysFromString(time):
    dateFormat = re.search("\d\d\d\d-\d\d-\d\d",time)
    if(dateFormat):
        tempString = time[8:10]
        result = int(tempString)
    else:
        if(len(time) == 10):
            tempString = time[0:2]
            result = int(tempString)
        else:
            tempString = time[0:1]
            result = int(tempString)
    return result

###Gets the hours from String
def getHoursFromString(time):
    if(len(time) == 8):
        tempString = time[0:2]
        result = int(tempString)
    elif(len(time) == 1):
        result = 0
    else:
        tempString = time[0:1]
        result = int(tempString)
    return result

###Gets the minutes from String
def getMinutesFromString(time):
    if(len(time) == 8):
        tempString = time[3:5]
        result = int(tempString)
    elif(len(time) == 1):
        result = 0
    else:
        tempString = time[2:4]
        result = int(tempString)
    return result

#Checks if it was a RA event
def getRAevent(project):
    if(re.search("r\D\d{2}\w{2}",project) or
       re.search("^RA",project)):
        return True
    else:
        return False

#Checks if it was an EVN event
def getEVNevent(project):
    if(re.search("^\w{2}\d{3}\w?",project) or
       re.search("FUS",project) or
       re.search("n\d{2}\w+\d",project) or
       re.search("EVN",project)):
        return True
    else:
        return False

#Checks if it was a sun event
def getSUNevent(project):
    if(re.search("Sun",project) or
       re.search("SUN",project)):
        return True
    else:
        return False

#Checks if it was an IVS event
def getIVSevent(project):
    if(re.search("IVS",project)):
        return True
    else:
        return False

#Checks if it was an NKA event
def getNKAevent(project):
    if(re.search("NKA",project)):
        return True
    else:
        return False

#Checks if it was a VIRAC event
def getVIRACevent(project):
    if(re.search("cepa",project) or
       re.search("VIRAC",project)):
        return True
    else:
        return False

#Checks if it was an EOFUS event
def getEOFUSevent(project):
    if(re.search("EOFUS",project)):
        return True
    else:
        return False

#Checks if it was an eVLBI event
def geteVLBIevent(project):
    if(re.search("eVLBI",project) or
       re.search("e\w\d{3}\w",project)):
        return True
    else:
        return False

#Checks if it was an AGN event
def getAGNevent(project):
    if(re.search("AGN",project)):
        return True
    else:
        return False

#Checks if it was a maser event
def getMASERevent(project):
    if(re.search("maser",project) or
       re.search("m\d{3}",project)):
        return True
    else:
        return False

#Checks which event was in the cell and counts it in an aray
def countingEvents(projectName,specProjectName, allEvents, projects, timeSpent):
    if(getRAevent(projectName)):
        allEvents[0][1] += 1
        allEvents[0][2] += timeSpent
        projects[0] += 1
    elif(getVIRACevent(projectName)):
        if(getMASERevent(specProjectName)):
            allEvents[3][1] += 1
            allEvents[3][2] += timeSpent
            projects[3] += 1
        elif(getAGNevent(specProjectName)):
            allEvents[4][1] += 1
            allEvents[4][2] += timeSpent
            projects[4] += 1
        else:
            allEvents[1][1] += 1
            allEvents[1][2] += timeSpent
            projects[1] += 1
    elif(getEVNevent(projectName)):
        allEvents[2][1] += 1
        allEvents[2][2] += timeSpent
        projects[2] += 1
    elif(getNKAevent(projectName)or getNKAevent(specProjectName)):
        allEvents[5][1] += 1
        allEvents[5][2] += timeSpent
        projects[5] += 1
    elif(getSUNevent(projectName)or getSUNevent(specProjectName)):
        allEvents[6][1] += 1
        allEvents[6][2] += timeSpent
        projects[6] += 1
    elif(getEOFUSevent(projectName)or getEOFUSevent(specProjectName)):
        allEvents[7][1] += 1
        allEvents[7][2] += timeSpent
        projects[7] += 1
    elif(geteVLBIevent(projectName)or geteVLBIevent(specProjectName)):
        allEvents[8][1] += 1
        allEvents[8][2] += timeSpent
        projects[8] += 1
    elif(getIVSevent(projectName)or getIVSevent(specProjectName)):
        allEvents[9][1] += 1
        allEvents[9][2] += timeSpent
        projects[9] += 1

###Calculates the total time spent on event in minutes 
def timeSpentOnEvent(startTimeDate, startTimeTime, endTimeDate, endTimeTime, year):
    longYearMonths = [31,29,31,30,31,30,31,31,30,31,30,31]
    shortYearMonths = [31,28,31,30,31,30,31,31,30,31,30,31]

    longYear = False
    if(year == 2016 or year == 2020):
        longYear = True
    startMonth = getMonthFromString(str(startTimeDate))
    endMonth = getMonthFromString(str(endTimeDate))
    
    startDay = getDaysFromString(str(startTimeDate))
    endDay = getDaysFromString(str(endTimeDate))
    if(startMonth < endMonth):
        if(longYear):
            endDay += longYearMonths[startMonth-1]
        else:
            endDay += shortYearMonths[startMonth-1]

    startHours = getHoursFromString(str(startTimeTime))
    endHours = getHoursFromString(str(endTimeTime))

    if(startDay < endDay):
        dayDifference = endDay - startDay
        endHours += 24*dayDifference

    startMinutes = getMinutesFromString(str(startTimeTime))
    endMinutes = getMinutesFromString(str(endTimeTime))
    if(endMinutes < startMinutes):
        endMinutes += 60
        startHours += 1

    totalHours = endHours - startHours
    totalMinutes = endMinutes - startMinutes
    totalTimeSpent = totalHours*60 + totalMinutes

    return totalTimeSpent

###Gets which type of date was in string
def getDateFromRegex(time):
    tempTime = ""
    if(re.search("^\d\d.\d\d.\d\d\d\d",time)):
        tempTime = re.findall("^\d\d.\d\d.\d\d\d\d",time)
    elif(re.search("^\d.\d\d.\d\d\d\d",time)):
        tempTime = re.findall("^\d.\d\d.\d\d\d\d", time)
    elif(re.search("^\d\d\d\d-\d\d-\d\d",time)):
        tempTime = re.findall("^\d\d\d\d-\d\d-\d\d", time)
        

    return tempTime

###Gets which type of time was in string
def getTimeFromRegex(time):
    tempTime = ""
    
    if(re.search("\d\d:\d\d:\d\d",time)):
        tempTime = re.findall("\d\d:\d\d:\d\d",time)
    elif(re.search("\d:\d\d:\d\d",time)):
        tempTime = re.findall("\d:\d\d:\d\d", time)
        
    return tempTime

###splits the string in correct type of date
def splitStringDate(date):
    result = ""
    if(len(date) == 14):
        result = date[2:12]
    elif(len(date) == 13):
        result = date[2:11]
    return result

###Splits the string in correct type of time
def splitStringTime(time):
    result = ""
    if (len(time) == 11):
        result = time[2:9]
    else:
        result = time[2:10]
    return result

###Returns a string of how much time was spent on the event
###1 hour 30 minutes (example)
def timeSpentOnEventString(totalTime):
    result = ""
    hours = totalTime/60
    hours = int(hours)

    realMinutes = totalTime%60

    if(hours > 0):
        if(hours == 1):
            result += str(hours) + " Hour "
        else:
            result += str(hours) + " Hours "

    if(realMinutes > 0):
        if(realMinutes == 1):
            result += str(realMinutes) + " Minute "
        else:
            result += str(realMinutes) + " Minutes "
    
    return result

###Counts all the operators and their time spent on the event
def countingOperatorsInEvents(allOperators, string, timeSpent):
    if(re.search("K.Prūsis", string) or
       re.search("K.Prusis", string) or
       re.search("Kaspars",string)):
        allOperators[0][1] += timeSpent
    if(re.search("A.Nikolajevs",string)):
        allOperators[1][1] += timeSpent
    if(re.search("A.Orbidans",string) or
       re.search("A.Orbidāns",string) or
       re.search("Artūrs",string) or
       re.search("Arturs",string)):
        allOperators[2][1] += timeSpent
    if(re.search("E.Visockis",string)):
        allOperators[3][1] += timeSpent
    if(re.search("M.Bleiders",string) or
       re.search("M. Bleiders",string) or
       re.search("Marcis",string)):
        allOperators[4][1] += timeSpent
    if(re.search("A.Aberfelds",string) or
       re.search("Artis",string)):
        allOperators[5][1] += timeSpent
    if(re.search("Vl.Bezrukovs",string) or
       re.search("Vladislavs",string)):
        allOperators[6][1] += timeSpent
    if(re.search("D.Bezrukovs",string) or
       re.search("Dmitrijs", string)):
        allOperators[7][1] += timeSpent
    if(re.search("R.Rasmanis",string)):
        allOperators[8][1] += timeSpent
    if(re.search("E.Brēdiķis",string) or
       re.search("E.Bredikis",string)):
        allOperators[9][1] += timeSpent
    if(re.search("R.Dirda",string)):
        allOperators[10][1] += timeSpent

#Checks if the event was successful or not
def isEventSuccessful(status):
    if(re.search("success",status) or
       re.search("partly",status) or
       re.search("sucess",status)):
        return True
    else:
        return False

#Checks if RT-32 telescope was used
def isRT32used(shell):
    if(re.search("RT-32",shell) or
       re.search("RT32",shell) or
       re.search("RtT-32",shell) or
       re.search("RT_32",shell)):
        return True
    else:
        return False
    
#Checks if RT-16 telescope was used
def isRT16used(shell):
    if(re.search("RT-16",shell) or
       re.search("RT16",shell) or
       re.search("RT_16",shell)):
        return True
    else:
        return False

#Returns the array of all the operators used
def getAllOperators():
    return allOperators

if __name__ == '__main__':
    main(year, startMonthInt, endMonthInt, projectInt)
    
