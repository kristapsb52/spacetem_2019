### this file is quickstart.py from google's examples modified by Ali Nuri SEKER
### the original could be found at https://developers.google.com/calendar/quickstart/python
### comments made by me will be marked with triple hash(#)
### this method of access can be used in any other type of google api's also


from __future__ import print_function
import httplib2
import os
import re

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
### this import is for service account. install api page -> https://developers.google.com/api-client-library/python/apis/calendar/v3
### $ pip install --upgrade google-api-python-client
from google.oauth2 import service_account

import datetime

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
### this is the path to the JSON file you provided -> https://console.developers.google.com/iam-admin/serviceaccounts/
SERVICE_ACCOUNT_FILE = 'service.json'

allEventsInCalendar = []
###Array for every operator found in all of the readings(name, time)
operatorsFull = [["A.Nikolajevs", 0],
                ["K.Prūsis", 0],
                ["R.Rasmanis", 0],
                ["E.Visockis", 0],
                ["M.Bleiders", 0],
                ["Ar.Bērziņš", 0],
                ["A.Orbidāns", 0],
                ["A.Aberfelds", 0],
                ["R.Dirda", 0],
                ["E.Brēdiķis", 0],
                ["Vl.Bezrukovs",0],
                ["Ivars Š", 0],
                ["Kārlis B", 0],
                ["Agris B",0],
                ["Mārcis D", 0],
                ["Dmitrijs",0],
                ["Normunds",0]]

###Array for every event found in all of the readings(name, count, time)
allEvents = [["RA events", 0, 0],
            ["VIRAC events", 0, 0],
            ["EVN events", 0, 0],
            ["MASER events", 0, 0],
            ["AGN events", 0, 0],
            ["TEST events", 0, 0],
            ["GMP events", 0, 0],
            ["g107 events", 0, 0],
            ["other events", 0, 0]]

###Array for telescopes and their time that was used (name, time)
telescopeUsage = [["RT-32", 0],
                  ["RT-16", 0],
                  ["No data", 0]]

###Returns the array with all events
def getAllEvents():
    return allEvents

### this function is used for service account credentials
def get_service_credentials():
    credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=[SCOPES])
    return credentials

def main():
    credentials=get_service_credentials()
    service = discovery.build('calendar', 'v3', credentials=credentials)
### after this line the usage is same as quickstart.py. only modification was made to the calendarId

    startDate = datetime.date(2018, 1, 1).isoformat()
    eventCountInCalendar = getEventCountInCalendar(2018, 3, 5, service)
    eventList = service.events().list(calendarId='2k1tq4bc5nnqsoso02c7tkv7gc@group.calendar.google.com', timeMin=startDate, maxResults=eventCountInCalendar, singleEvents=True,orderBy='startTime').execute()
    eventArray = eventList.get('items', [])
    getAllEventsInfo(eventArray, specific)


def getEvents(year, startMonth, endMonth, specific, method):
    credentials=get_service_credentials()
    service = discovery.build('calendar', 'v3', credentials=credentials)
### after this line the usage is same as quickstart.py. only modification was made to the calendarId


    ###TODO make it so it just calls the function to make the array, without the need to ask user for inputs
    ###TODO make function
    startDate = datetime.datetime(year, startMonth, 1, 0, 0, 0, 0).isoformat() + 'Z'
    eventCountInCalendar = getEventCountInCalendar(year, startMonth, endMonth, service)
    eventList = service.events().list(calendarId='2k1tq4bc5nnqsoso02c7tkv7gc@group.calendar.google.com', timeMin=startDate, maxResults=eventCountInCalendar, singleEvents=True,orderBy='startTime').execute()
    eventArray = eventList.get('items', [])
    getAllEventsInfo(eventArray, specific, method)

###Returns the array of the telescopes and their used time
def getTelescopeUsageFromCalendar():
    return telescopeUsage

###Gets the total amount of time that was spent on all of the events
def getTotalMinutesFromEvents(projectInt):
    result = 0
    if(projectInt == None):
        for event in allEvents:
            result += event[2]
    else:
        result = allEvents[projectInt-1][2]
    return result

###Returns the array with all events for comparison
def getAllEventsFromCalendar():
    return allEventsInCalendar

#Gets the year from event string
def getYearFromEvent(eventDate):
    tempString = eventDate[0:4]
    result = int(tempString)
    return result

#Gets the month from event string
def getMonthFromEvent(eventDate):
    tempString = eventDate[5:7]
    result = int(tempString)
    return result

#Gets the day from event string
def getDayFromEvent(eventDate):
    tempString = eventDate[8:10]
    result = int(tempString)
    return result

#Gets hours from event string
def getHoursFromEvent(eventDate):
    result = 0
    if(len(eventDate) > 10):
        tempString = eventDate[11:13]
        result = int(tempString)
    return result

#Gets minutes from event string
def getMinutesFromEvent(eventDate):
    result = 0
    if(len(eventDate) > 10):
        tempString = eventDate[14:16]
        result = int(tempString)
    return result

#Gets the total amount of events in calendar
def getEventCountInCalendar(year, startMonth, endMonth, service):
    eventDate = datetime.datetime(year, startMonth, 1, 0, 0, 0, 0).isoformat() + 'Z'
    eventList = service.events().list(calendarId='2k1tq4bc5nnqsoso02c7tkv7gc@group.calendar.google.com', timeMin=eventDate, maxResults=500, singleEvents=True,orderBy='startTime').execute()
    eventArray = eventList.get('items', [])

    eventCount = 0;
    yearNode = year
    dayNode = 1
    Break = 0
    while (Break == 0):
        tempCount = 0
        arrayLength = len(eventArray)
        for event in eventArray:
            tempCount += 1
            tempTime = event['start'].get('dateTime', event['start'].get('date'))
            monthNode = getMonthFromEvent(tempTime)
            yearNode = getYearFromEvent(tempTime)
            if(endMonth != 12):
                if(monthNode > endMonth):
                    eventArray.clear()
                    Break = 1
                    break
            if(yearNode > year):
                eventArray.clear()
                Break = 1
                break
            eventCount += 1
            if(tempCount == 500):
                yearNode = getYearFromEvent(tempTime)
                monthNode = getMonthFromEvent(tempTime)
                dayNode = getDayFromEvent(tempTime)
                break
        nextTimeNode = datetime.datetime(yearNode, monthNode, dayNode, 1, 1, 1, 1).isoformat() + 'Z'
        nextRealEventListNode = service.events().list(calendarId='2k1tq4bc5nnqsoso02c7tkv7gc@group.calendar.google.com', timeMin=nextTimeNode, maxResults=500, singleEvents=True,orderBy='startTime').execute()
        eventArray = nextRealEventListNode.get('items', [])        
    
    return eventCount
#Checks if the event is EVN
def allEVNevents(summary):
    if(re.search("\w{2}\d{3}\w?",summary) or
       re.search("\D\d{2}\w\d",summary) or
       re.search("\w\d{2}\D{2}\d",summary) or
       re.search("prec\d{2}",summary) or
       re.search("EVN",summary) or
       re.search("EOFUS",summary)):
        return True
    else:
        return False

#Checks if the event is RA
def allRaEvents(summary):
    if(re.search("radt07\w", summary) or
       re.search("radt08\w", summary) or
        re.search("raks12\w{2}", summary) or
        re.search("raks14\w{2}", summary) or
       re.search("raks14\w", summary) or
        re.search("rags14\w", summary) or
        re.search("raks16\w{2}", summary) or
        re.search("raks18\w{2}", summary) or
        re.search("rafs20\w", summary) or
        re.search("raks23\w{2}", summary) or
        re.search("raks25\w{2}", summary) or
        re.search("rags28\w{2}", summary) or
       re.search("rags28\w{2}", summary) or
        re.search("rags30\w", summary) or
       re.search("rags36\w",summary)):
        return True
    else:
        return False

#Checks if the event is Maser
def allMaserEvents(summary):
    if(re.search("maser",summary) or
       re.search("mazer",summary) or
       re.search("Maser",summary)):
        return True
    else:
        return False

#Checks if the event is a test
def alltestEvents(summary):
    if(re.search("test",summary) or
       re.search("tests",summary)):
        return True
    else:
        return False

#Checks if the event is agn
def allagnEvents(summary):
    if(re.search("AGN", summary)):
        return True
    else:
        return False

#Checks if the event is virac
def allviracEvents(summary):
    if(re.search("VIRAC",summary)):
        return True
    else:
        return False

#Checks if the event is gmp
def allgmpEvents(summary):
    if(re.search("GMP",summary)):
        return True
    else:
        return False

#Checks if the event is g107
def allg107Events(summary):
    if(re.search("[gG]107",summary)):
        return True
    else:
        return False

#Checks if the event was using RT-32 telescope
def isRT32used(summary):
    if(re.search("RT-32",summary) or
       re.search("RT32",summary) or
       re.search("rt-32",summary) or
       re.search("RT_32",summary) or
       re.search("rt_32",summary) or
       re.search("rt32",summary)):
        return True
    else:
        return False

#Checks if the event was using RT-16 telescope
def isRT16used(summary):
    if(re.search("RT-16",summary) or
       re.search("RT16",summary) or
       re.search("rt-16",summary) or
       re.search("rt16",summary) or
       re.search("RT_16",summary) or
       re.search("rt_16",summary)):
        return True
    else:
        return False
       
#Calculates the time spent on a single event from start of event till end of event
def timeSpentOnEventCalc(startTime, endTime):
    startTimeDay = getDayFromEvent(startTime)
    endTimeDay = getDayFromEvent(endTime)

    startTimeHours = getHoursFromEvent(startTime)
    endTimeHours = getHoursFromEvent(endTime)

    if(endTimeDay > startTimeDay):
        endTimeHours += 24

    startTimeMinutes = getMinutesFromEvent(startTime)
    endTimeMinutes = getMinutesFromEvent(endTime)

    if(startTimeMinutes > endTimeMinutes):
        endTimeMinutes += 60
        startTimeHours += 1

    totalHours = endTimeHours - startTimeHours
    
    totalMinutes = endTimeMinutes - startTimeMinutes

    totalTimeSpent = totalHours*60 + totalMinutes

    return totalTimeSpent

###returns the status for the event
def getEventStatus(description,summary):
    eventStatus = re.findall("[cC]ancel{1,2}ed",description) or re.findall("failed",description) or re.findall("[cC]ancel{1,2}ed",summary) or re.findall("failed",summary) or re.findall("cacncelled",description) or re.findall("CANCELED",description) or re.findall("failure", description) or re.findall("sucess", description) or re.findall("partly",description)

    return eventStatus
        
#Checks if the event was failed or not
def eventFailed(description,summary):
    if(re.search("[cC]ancel{1,2}ed",description) or
       re.search("failed",description) or
       re.search("[cC]ancel{1,2}ed",summary) or
       re.search("failed",summary) or
       re.search("cacncelled",description) or
       re.search("CANCELED",description) or
       re.search("failure", description) or
       re.search("sucess", description) or
       re.search("partly",description)):
        return True
    else:
        return False

#Checks all the operators in the description and adds the time that was spent on the event
def getAllOperatorsInString(operatorsFull, description, timeSpent):
    isA_Nikolajevs = re.search("Nikolajevs",description)
    if(isA_Nikolajevs):
        operatorsFull[0][1] += timeSpent
        
    isK_Prūsis = re.search("Prūsis",description)
    isK_Prūsis1 = re.search("Kaspars",description)
    if(isK_Prūsis or isK_Prūsis1):
        operatorsFull[1][1] += timeSpent
        
    isR_Rasmanis = re.search("Rasmanis",description)
    if(isR_Rasmanis):
        operatorsFull[2][1] += timeSpent

    isE_Visockis = re.search("Visockis",description)
    isE_Visockis1 = re.search("Edgars",description)
    if(isE_Visockis or isE_Visockis1):
        operatorsFull[3][1] += timeSpent
        
    isM_Bleiders = re.search("Bleiders",description)
    isM_Bleiders1 = re.search("Mārcis B",description)
    isM_Bleiders2 = re.search("Marcis B",description)
    isM_Bleiders3 = re.search("Marcis",description)
    isM_Bleiders4 = re.search("Mārcis",description)
    if(isM_Bleiders or isM_Bleiders1 or isM_Bleiders2 or isM_Bleiders3 or isM_Bleiders4):
        operatorsFull[4][1] += timeSpent

    isAr_Berzins = re.search("Bērziņš",description)
    isAr_Berzins1 = re.search("Arnis B",description)
    if(isAr_Berzins or isAr_Berzins1):
        operatorsFull[5][1] += timeSpent
        
    isA_Orbidans = re.search("Orbidans",description)
    isA_Orbidans1 = re.search("Arturs O",description)
    isA_Orbidans2 = re.search("Artūrs O",description)
    if(isA_Orbidans or isA_Orbidans1 or isA_Orbidans2):
        operatorsFull[6][1] += timeSpent
        
    isA_Aberfelds = re.search("Aberfelds",description)
    isA_Aberfelds1 = re.search("Artis",description)
    isA_Aberfelds2 = re.search("Arits",description)
    if(isA_Aberfelds or isA_Aberfelds1 or isA_Aberfelds2):
        operatorsFull[7][1] += timeSpent

    isR_Dirda = re.search("Dirda",description)
    if(isR_Dirda):
        operatorsFull[8][1] += timeSpent
        
    isE_Bredikis = re.search("Brēdiķis",description)
    isE_Bredikis1 = re.search("Bredikis",description)
    if(isE_Bredikis or isE_Bredikis1):
        operatorsFull[9][1] += timeSpent

    isVl_Bezrukovs = re.search("Bezrukovs",description)
    isVl_Bezrukovs1 = re.search("Vladislavs" ,description)
    if(isVl_Bezrukovs or isVl_Bezrukovs1):
        operatorsFull[10][1] += timeSpent

    isIvars_S = re.search("Ivars Š",description)
    isIvars_S1 = re.search("Ivars",description)
    if(isIvars_S or isIvars_S1):
        operatorsFull[11][1] += timeSpent

    isKarlis_B = re.search("Kārlis B",description)
    isKarlis_B1 = re.search("Karlis",description)
    if(isKarlis_B or isKarlis_B1):
        operatorsFull[12][1] += timeSpent

    isAgris_B = re.search("Agris B",description)
    isAgris_B1 = re.search("Agris",description)
    if(isAgris_B):
        operatorsFull[13][1] += timeSpent

    isMarcis_D = re.search("Marcis D", description)
    if(isMarcis_D):
        operatorsFull[14][1] += timeSpent

    isDmitrijs = re.search("Dmitrijs",description)
    isDmitrijs2 = re.search("Dmitris",description)
    if(isDmitrijs or isDmitrijs2):
        operatorsFull[15][1] += timeSpent

    isNormunds = re.search("Normunds",description)
    if(isNormunds):
        operatorsFull[16][1] += timeSpent

###Counts the events and adds the time spent on it
def countAllEvents(allEvents, description, project, timeSpentOnEvent):
    if(allRaEvents(description)):
        allEvents[0][1] += 1
        allEvents[0][2] += timeSpentOnEvent
        project[0] += 1
    elif(allviracEvents(description)):
        allEvents[1][1] += 1
        allEvents[1][2] += timeSpentOnEvent
        project[1] += 1
    elif(allEVNevents(description)):
        allEvents[2][1] += 1
        allEvents[2][2] += timeSpentOnEvent
        project[2] += 1
    elif(allMaserEvents(description)):
        allEvents[3][1] += 1
        allEvents[3][2] += timeSpentOnEvent
        project[3] += 1
    elif(allagnEvents(description)):
        allEvents[4][1] += 1
        allEvents[4][2] += timeSpentOnEvent
        project[4] += 1
    elif(alltestEvents(description)):
        allEvents[5][1] += 1
        allEvents[5][2] += timeSpentOnEvent
        project[5] += 1
    elif(allgmpEvents(description)):
        allEvents[6][1] += 1
        allEvents[6][2] += timeSpentOnEvent
        project[6] += 1
    elif(allg107Events(description)):
        allEvents[7][1] += 1
        allEvents[7][2] += timeSpentOnEvent
        project[7] += 1

###Returns a string of how much time was spent on an event(1 hour 30 minutes, for example)
def timeSpentOnEventString(totalTime):
    result = ""
    hours = totalTime/60
    realHours = int(hours)
    realMinutes = totalTime%60


    if(realHours > 0):
        if(realHours == 1):
            result += str(realHours) + " Hour "
        else:
            result += str(realHours) + " Hours "

    if(realMinutes > 0):
        if(realMinutes == 1):
            result += str(realMinutes) + " Minute "
        else:
            result += str(realMinutes) + " Minutes "
    
    return result

###Returns the array with all the operators
def getAllOperators():
    return operatorsFull

#Prints out all the necessary information about the event
def getAllEventsInfo(eventArray, specific, method):
    arrayLength = 0
    totalTime = 0
    tempCount = 0
    allEventCount = len(eventArray)

    print("Does this happen")
    for event in eventArray:
        wontWork = 0
        tempCount += 1
        startEvent = event['start'].get('dateTime', event['start'].get('date'))
        endEvent = event['end'].get('dateTime', event['end'].get('date'))
        timeSpentOnEvent = timeSpentOnEventCalc(startEvent, endEvent)
        
        try:
            summary = event['summary']
        except Exception as e:
            print("In summary: ",e)
            wontWork = 1
        try:
            descr = event.get('description')
##            print(descr)
            tempStatus = getEventStatus(descr, summary)
        except Exception as e:
            print("In description: ",e)
            wontWork = 1

        try:
            if(wontWork == 0):
                notWork = 0
                project = [0,0,0,0,0,0,0,0,0]
                countAllEvents(allEvents, summary, project, timeSpentOnEvent)
                if (specific != None):
                    if(project[specific-1] == 0):
                        notWork = 1

                if(notWork == 0):
                    if(method == True):
                        print(startEvent, " until ", endEvent, "Time spent: ", timeSpentOnEventString(timeSpentOnEvent))
                        print(summary)
                    if(isRT32used(summary) or isRT32used(descr)):
                        telescopeUsage[0][1] += timeSpentOnEvent
                    elif(isRT16used(summary) or isRT16used(descr)):
                        telescopeUsage[1][1] += timeSpentOnEvent
                    else:
                        telescopeUsage[2][1] += timeSpentOnEvent

                    ###Appends the array that is going to be used for comparison in different script
                    allEventsInCalendar.append([])
                    allEventsInCalendar[arrayLength].append(startEvent)
                    allEventsInCalendar[arrayLength].append(endEvent)
                    allEventsInCalendar[arrayLength].append(summary)
                    allEventsInCalendar[arrayLength].append(tempStatus)
                    arrayLength += 1
                    getAllOperatorsInString(operatorsFull, descr, timeSpentOnEvent)
                    totalTime += timeSpentOnEvent
        except Exception as e:
            print(e)
            
##    print("Total time spent on these events: " + timeSpentOnEventString(totalTime))
##    if(specific == None):
##        for event in allEvents:
##            print(event[0] , event[1])
##    else:
##        print(allEvents[specific-1][0], " ", allEvents[specific-1][1])
##    print ("------------------")
    print ("All Operators that got involved in atleast one of the readings:")
    for operator in operatorsFull:
        minutes = int(operator[1])
        if(minutes > 0):
            print(operator[0] + " has spent " + timeSpentOnEventString(operator[1]))

    print("-------------------")
    print("All event time spent:")
    for event in allEvents:
        if(event[2] > 0):
            print(event[0] + " time spent " + timeSpentOnEventString(event[2]))

            
if __name__ == '__main__':
    main()

